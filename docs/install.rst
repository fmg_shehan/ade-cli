.. _install:

Installation
------------

Requirements
^^^^^^^^^^^^

A Linux distribution with the following programs installed:
   * Docker 19.03 or newer, follow the `instructions for your Linux distribution`_
   * ``nvidia-docker2``, if the host machine has an NVIDIA graphics card
      * In the `future`_, ``ade-cli`` will support Docker's ``--gpus`` command, so
        ``nvidia-docker2`` will not be necessary. See the `NVIDIA Docker`_ project
        for more details


Installation
^^^^^^^^^^^^

As of version 4.0, ADE is distributed as a single, statically-linked binary for
x86_64 and aarch64. Download the binary from the `Releases`_ page of the
``ade-cli`` project, and install it in PATH.

For information on how to install ADE (``ade-cli``, ADE base image, and ADE volumes)
on an offline machine, see :ref:`offline-install`.

.. toctree::
   :maxdepth: 4
   :hidden:

   offline-install


Update
^^^^^^

To update ``ade-cli``, run ``ade update-cli``. If a newer version is available,
``ade`` will prompt for confirmation, download the new version, and replace itself.


Autocompletion
^^^^^^^^^^^^^^

To enable autocompletion, add the following to your ``.zshrc`` or ``.bashrc``:

   .. literalinclude:: ../completion.sh
      :language: shell
      :lines: 3-


See :ref:`usage` for next steps.

.. _future: https://gitlab.com/ApexAI/ade-cli/issues/52
.. _Nvidia Docker: https://github.com/NVIDIA/nvidia-docker
.. _Releases: https://gitlab.com/ApexAI/ade-cli/-/releases
.. _instructions for your Linux distribution: https://docs.docker.com/install/#server
