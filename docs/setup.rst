.. _setup:

Setup
-----

.. toctree::
   :maxdepth: 2
   :hidden:

   create-custom-base-image
   aderc-file
   create-custom-volume


To setup ADE for a project, prepare two main components:

1. A **base-image** which is the Docker image that provides all the programs and dependencies
   for the project. See :ref:`create-custom-base-image` for details.
2. An ``.aderc`` file which specifies the Docker images to start and any additional
   ``docker run`` arguments. See :ref:`aderc-file` for details.

In addition to a **base-image** and an ``.aderc`` file, it is recommended to use **volumes**
to provide large, stand-alone programs and libraries. **Volumes** allow the project to update
different components of ADE without requiring users to download an increasingly large image.
See :ref:`create-custom-volume` for details.


External Interfaces
===================


.. toctree::
   :maxdepth: 2
   :hidden:

   start-ade-macvlan
   mounting-ports
   mounting-usb
   nvidia-docker


ADE can also be configured to use external interfaces (network devices, ports, and/or USB
devices) to get, for example, information from sensors:

- :ref:`start-ade-macvlan` and :ref:`mounting-ports` explain how to configure the network
  interfaces of ADE
- :ref:`mounting-usb` explains how to make USB devices available inside ADE
- :ref:`nvidia-docker` explains how to integrate NVidia Docker with ADE

