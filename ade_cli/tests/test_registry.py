# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=missing-module-docstring,missing-function-docstring

import pytest

from ade_cli.registry import Image


def test_image():
    image = Image('some.registry:5000/name/space/image:foo')
    assert image.fqn == 'some.registry:5000/name/space/image:foo'
    assert image.registry == 'some.registry:5000'
    assert image.namespace == 'name/space'
    assert image.name == 'image'
    assert image.tag == 'foo'
    assert image.api_url == 'https://some.registry:5000/v2'
    assert image.tags_url == 'https://some.registry:5000/v2/name/space/image/tags/list'
    assert repr(image) == "Image('some.registry:5000/name/space/image:foo')"


@pytest.mark.parametrize('url,registry,namespace,name,tag', [
    ['name',
     None,
     None,
     'name',
     'latest'],
    ['na.me',
     None,
     None,
     'na.me',
     'latest'],
    ['na.me:tag',
     None,
     None,
     'na.me',
     'tag'],
    ['registry.com/namespace/subnamespace/name:tag',
     'registry.com',
     'namespace/subnamespace',
     'name',
     'tag'],
    ['registry.com/namespace/name:tag',
     'registry.com',
     'namespace',
     'name',
     'tag'],
    ['registry.com:1234/namespace/subnamespace/name:tag',
     'registry.com:1234',
     'namespace/subnamespace',
     'name',
     'tag'],
    ['registry.com:1234/namespace/name:tag',
     'registry.com:1234',
     'namespace',
     'name',
     'tag'],
    ['registry.com:1234/namespace/na.me:tag',
     'registry.com:1234',
     'namespace',
     'na.me',
     'tag'],
])
def test_image_urls(url, registry, namespace, name, tag):
    image = Image(url)
    assert image.registry == registry
    assert image.namespace == namespace
    assert image.name == name
    assert image.tag == tag
