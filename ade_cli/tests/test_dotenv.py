# Copyright 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Tests for dotenv file parser"""

from pathlib import Path

import pytest

from ade_cli.dotenv import UnterminatedDoubleQuotes
from ade_cli.dotenv import UnterminatedSingleQuotes
from ade_cli.dotenv import InvalidCharacter, MissingValue, read_vars
from ade_cli.dotenv import expand_vars, load_dotenv


def test():
    """Test reading variables from dotenv file."""
    rv = read_vars("""
# ignore
FOO=1
  BAR=2  BAZ=3  # also IGNORE=1
export export A=A
EMPTY_VALUE= EMPTY2=
EMPTY3=\\

NOT_EMPTY=\\
#"foo bar"
# last line comment EOF without EOL""")
    assert rv == {
        'A': 'A',
        'BAR': '2',
        'BAZ': '3',
        'EMPTY_VALUE': '',
        'EMPTY2': '',
        'EMPTY3': '',
        'FOO': '1',
        'NOT_EMPTY': '#foo bar',
    }

    rv = read_vars("FOO=1")
    assert rv == {'FOO': '1'}

    rv = read_vars("ESC=\\$")
    assert rv == {'ESC': '$'}

    rv = read_vars("""
QUOTED='
  "a"
  b
'"'"''""''"
  \\$\\a\\
"
""")
    assert rv == {'QUOTED': '\n  "a"\n  b\n\'\n  $\\a'}

    with pytest.raises(InvalidCharacter) as einfo:
        read_vars(" abc=1")
    assert einfo.value.args == ('a', 1)

    with pytest.raises(InvalidCharacter) as einfo:
        read_vars("INVaLID")
    assert einfo.value.args == ('a', 3)

    with pytest.raises(InvalidCharacter) as einfo:
        read_vars("FOO=$")
    assert einfo.value.args == ('$', 4)

    with pytest.raises(InvalidCharacter) as einfo:
        read_vars("FOO=`")
    assert einfo.value.args == ('`', 4)

    with pytest.raises(InvalidCharacter) as einfo:
        read_vars('FOO="`"')
    assert einfo.value.args == ('`', 5)

    with pytest.raises(UnterminatedDoubleQuotes) as einfo:
        read_vars('FOO="')
    assert einfo.value.args == ('FOO',)

    with pytest.raises(UnterminatedSingleQuotes) as einfo:
        read_vars("FOO='")
    assert einfo.value.args == ('FOO',)

    with pytest.raises(MissingValue) as einfo:
        read_vars("WRONG")
    assert einfo.value.args == ('WRONG',)

    with pytest.raises(MissingValue) as einfo:
        read_vars("WRONG FOO=1")
    assert einfo.value.args == ('WRONG',)


def test_variable_expansion():
    """Test variable expansion."""
    rv = read_vars('VAR="${FOO}${BAR:-default value}"')
    assert rv == {'VAR': '${FOO}${BAR:-default value}'}

    string = rv['VAR']
    rv = expand_vars(string, {'FOO': '1', 'BAR': '2'})
    assert rv == '12'
    rv = expand_vars(string, {'FOO': '1'})
    assert rv == '1default value'


def test_load_dotenv(tmpdir):
    """Test loading variables into environment."""
    tmpdir = Path(str(tmpdir))  # pytest does not return pathlib objects, at least on py35
    subdir = tmpdir / 'subdir'
    subdir.mkdir()
    envfile = (tmpdir / '.env')
    envfile.write_text("""
FOO=foo
BAR=bar
VAR="${FOO} ${BAR:-defaultbar} ${XYZ:-defaultxyz}"
""", encoding='utf-8')
    env = {
        'ABC': 'envabc',
        'FOO': 'envfoo',
    }
    loaded_file = load_dotenv(cwd=subdir, env=env)
    assert loaded_file == envfile
    assert env == {
        'ABC': 'envabc',
        'BAR': 'bar',
        'FOO': 'envfoo',
        'VAR': 'envfoo bar defaultxyz'
    }
