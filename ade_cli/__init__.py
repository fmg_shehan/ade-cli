# Copyright 2016 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Agile Development Environment"""

__version__ = '4.1.0dev'
