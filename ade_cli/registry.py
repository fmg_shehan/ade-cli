# Copyright 2017 - 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Communicate with docker registry"""

import asyncio
from subprocess import CalledProcessError, DEVNULL, PIPE

import aiohttp

from .credentials import get_credentials
from .utils import run


class NoSuchImage(Exception):
    """Requested image is not available"""


class NoSuchTag(Exception):
    """Requested tag does not exist for image.

    raise NoSuchTag(img, tag, available_tags)
    """


class TokenError(Exception):
    """Error obtaining token for URL. Auth data is wrong"""


class Image:
    """Data model for an image in a docker registry."""
    registry = None
    namespace = None
    name = None
    tag = 'latest'

    @property
    def fqn(self):
        """Fully qualified name of a docker image including the tag"""
        if self.registry:
            return f'{self.registry}/{self.namespace}/{self.name}:{self.tag}'
        return f'{self.name}:{self.tag}'

    @property
    def api_url(self):
        """URL for API of the image's registry"""
        if self.registry:
            return f'https://{self.registry}/v2'
        return None

    @property
    def tags_url(self):
        """URL for API endpoint for list of tags for image"""
        if self.registry:
            return f'https://{self.registry}/v2/{self.namespace}/{self.name}/tags/list'
        return None

    def __init__(self, fqn):
        components = fqn.split('/')
        if ':' in components[-1]:
            self.name, self.tag = components.pop().split(':')
        else:
            self.name = components.pop()

        if components and '.' in components[0]:
            self.registry = components.pop(0)

        if components:
            self.namespace = '/'.join(components)

    def __repr__(self):
        return "Image('{}')".format(self.fqn)


async def adjust_tag(client, image, selectors):
    """Adjust image tag according to selectors and availability"""
    tags = await get_tags(client, image)
    for name, tag in selectors:
        if image.name == name:
            if tag not in tags:
                raise NoSuchTag(image, tag, sorted(tags))
            image.tag = tag
            break
        if name is None and tag in tags:
            image.tag = tag
            break


async def adjust_tags(images, selectors):
    """Adjust tags of images according to selectors and availability"""
    async with aiohttp.ClientSession() as client:
        reqs = [adjust_tag(client, img, selectors) for img in images]
        await asyncio.gather(*reqs)


async def get(client, url, registry, accept=None, stream=False):
    """Get url, implicitly handling authentication"""
    headers = {}
    accept = accept or 'application/json'
    if accept:
        headers['Accept'] = accept

    resp = await client.get(url, headers=headers)
    if resp.status == 401:
        token = await _authtoken(client, resp.headers, registry)
        headers['Authorization'] = 'Bearer {}'.format(token)
        resp = await client.get(url, headers=headers)

    if stream:
        return resp.content
    return await resp.json(content_type=accept)


def login(img):
    """Login into registry serving image"""
    username, token = get_credentials(img.registry).values()
    run(['docker', 'login', '--username', username, '--password-stdin', img.registry],
        input=token.encode('utf-8'), check='exit')


def pull_if_missing(image):
    """Pull an image if it does not exists locally"""
    try:
        run(['docker', 'image', 'inspect', '--format', '{{.Id}}', image.fqn],
            stdout=DEVNULL, stderr=DEVNULL)
    except CalledProcessError:
        pass
    else:
        return

    try:
        run(['docker', 'pull', image.fqn])
    except CalledProcessError:
        login(image)
        run(['docker', 'pull', image.fqn], check='exit')


async def update_image(img):
    """Update an image from docker registry"""
    try:
        old_id = run(['docker', 'image', 'inspect', '--format', '{{.Id}}', img.fqn],
                     stdout=PIPE, stderr=DEVNULL).stdout.decode('utf-8').strip()
    except CalledProcessError:
        old_id = None

    try:
        run(['docker', 'pull', img.fqn])
    except CalledProcessError:
        login(img)
        run(['docker', 'pull', img.fqn], check='exit')

    new_id = run(['docker', 'image', 'inspect', '--format', '{{.Id}}', img.fqn],
                 stdout=PIPE).stdout.decode('utf-8').strip()
    return new_id != old_id


async def get_tags(client, image):
    """Get list of tags for image from registry"""
    data = await get(client, image.tags_url, registry=image.registry)
    try:
        return [x for x in data['tags'] if not x.startswith('commit-')]
    except KeyError:
        raise NoSuchImage(image)


async def _authtoken(client, headers, registry):
    info = {
        k: v.split('"')[1] for k, v in [
            x.split('=') for x in headers['Www-Authenticate'].split()[1].split(',')
        ]
    }
    info['client_id'] = 'ade'
    auth = aiohttp.BasicAuth(*get_credentials(registry).values())
    resp = await client.get(info.pop('realm'), auth=auth, params=info)
    resp = await resp.json()
    try:
        token = resp['token']
    except KeyError:
        raise TokenError()
    return token
